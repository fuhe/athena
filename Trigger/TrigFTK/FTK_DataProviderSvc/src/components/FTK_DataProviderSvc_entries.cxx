
#include "FTK_DataProviderSvc/FTK_DataProviderSvc.h"
#include "FTK_DataProviderSvc/FTK_UncertaintyTool.h"
#include "FTK_DataProviderSvc/FTKFastDataProviderSvc.h"

DECLARE_COMPONENT( FTK_DataProviderSvc )
DECLARE_COMPONENT( FTK_UncertaintyTool )
DECLARE_COMPONENT( FTKFastDataProviderSvc )
